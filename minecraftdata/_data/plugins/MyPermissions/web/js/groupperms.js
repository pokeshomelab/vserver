load();

function load() {
    $.ajax({
        url: '',
        type: "GETGROUPS",
        success: function(result){
            $('#groupSelect').html(result);
            getGroupPerms($('#groupSelect').val());
            console.log(result);
        },
        error: function(error) {
            $('.panel').html('Error');
        }
    })
}

function getGroupPerms(group) {
    $.ajax({
        url: group,
        type: "GETGPERMS",
        success: function(result){
            $('#permsPanel').val(result);
            console.log(result);
        },
        error: function(error) {
            $('.panel').html('Error');
        }
    })
}

function sendGPerms() {
    $.ajax({
        url: $('#groupSelect').val(),
        type: "SETGPERMS",
        data: $('#permsPanel').val() + '\n',
        success: function(result){
            console.log(result);
        },
        error: function(error) {
            $('.panel').html('Error');
        }
    })
    location.reload();
}