isLoggedIn();
function isLoggedIn() {
    $('.grid').hide();
    $.ajax({
        url: '',
        type: "LOGGEDIN",
        success: function(result){
            if(result.startsWith('f')) {
                openHome();
            }
            console.log(result);
        },
        error: function(error) {
            $('.panel').html('Error');
        }
    })
}

function openHome() {
    window.open("index.html","_self")
}

function openCreate() {
    $('.frame').attr("src", "creategroup.html");
}

function openConfig() {
    $('.frame').attr("src", "configgroups.html");
}

function openPerms() {
    $('.frame').attr("src", "groupperms.html");
}

function openNegs() {
    $('.frame').attr("src", "groupnegs.html");
}

function openPlayers() {
    $('.frame').attr("src", "grouppl.html");
}