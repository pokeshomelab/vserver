isLoggedIn();

function openGroups() {
    window.open("group.html","_self")
}

function openPlayers() {
    window.open("players.html","_self")
}

function isLoggedIn() {
    $('.grid').hide();
    $.ajax({
        url: '',
        type: "LOGGEDIN",
        success: function(result){
            if(result.startsWith('f')) {
                console.log('works');
                $('#login').show();
                $('.grid').hide();
            } else {
                $('#login').hide();
                $('.grid').show();
            }
            console.log(result);
        },
        error: function(error) {
            $('.panel').html('Error');
        }
    })
}

function login() {
    var s = $('#username').val() + '\n' + $('#password').val();
    $.ajax({
        url: '',
        data: s,
        type: "LOGIN",
        success: function(result){
            console.log(result);
        },
        error: function(error) {
            $('.panel').html('Error');
        }
    })
    location.reload();
}

function logout() {
    $.ajax({
        url: '',
        type: "LOGOUT",
        success: function(result){
            console.log(result);
        },
        error: function(error) {
            $('.panel').html('Error');
        }
    })
    location.reload();
}