function create() {
    $.ajax({
        url: $('#name').val(),
        type: "CREATE",
        success: function(result){
            console.log(result);
            if(result == "success") {
                $('#status').attr("style","color: #00EE00FF;");
                $('#status').html("Group created!");
                location.reload();
            } else {
                $('#status').attr("style","color: #EE0000FF;");
                $('#status').html("Creating group failed!");
                location.reload();
            }
        },
        error: function(error) {
            $('.panel').html('Error');
        }
    })
}