load();

function load() {
    $.ajax({
        url: '',
        type: "GETGROUPS",
        success: function(result){
            $('#groupSelect').html(result);
            getConfig($('#groupSelect').val());
            console.log(result);
        },
        error: function(error) {
            $('.panel').html('Error');
        }
    })
}

function getConfig(group) {
    $.ajax({
        url: group,
        type: "GETCONFIG",
        success: function(result){
            const lines = result.split('\n');
            $('#rankSelect').val(lines[0]);
            $('#prefix').val(lines[1]);
            $('#suffix').val(lines[2]);
            if(lines[3].startsWith('t')) {
                $('#op').prop("checked", true);
            } else {
                $('#op').prop("checked", false);
            }
            console.log(result);
        },
        error: function(error) {
            $('.panel').html('Error');
        }
    })
}

function save() {
    var s = '';

    s += $('#rankSelect').val() + '\n';
    s += $('#prefix').val() + '\n';
    s += $('#suffix').val() + '\n';
    
    if($('#op').prop("checked")) {
        s += 'true\n';
    } else {
        s += 'false\n';
    }

    $.ajax({
        url: $('#groupSelect').val(),
        type: "SETCONFIG",
        data: s,
        success: function(result){
            console.log(result);
            if(result == "success") {
                $('#status').html("Saved successfully!");
                $('#status').attr("style", "color: #00EE00FF;");
            } else {
                $('#status').html("Saving failed!");
                $('#status').attr("style", "color: #EE0000FF;");
            }
        },
        error: function(error) {
            $('.panel').html('Error');
        }
    })
}

function del() {
    $.ajax({
        url: $('#groupSelect').val(),
        type: "DELGROUP",
        success: function(result){
            console.log(result);
            if(result == "success") {
                location.reload();
                $('#status').html("Deleted group successfully!");
                $('#status').attr("style", "color: #00EE00FF;");
            } else {
                location.reload();
                $('#status').html("Deleting group failed!");
                $('#status').attr("style", "color: #EE0000FF;");
            }
        },
        error: function(error) {
            $('.panel').html('Error');
        }
    })
}