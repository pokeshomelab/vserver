isLoggedIn();
function isLoggedIn() {
    $('.grid').hide();
    $.ajax({
        url: '',
        type: "LOGGEDIN",
        success: function(result){
            if(result.startsWith('f')) {
                openHome();
            }
            console.log(result);
        },
        error: function(error) {
            $('.panel').html('Error');
        }
    })
}
function openHome() {
    window.open("index.html","_self")
}
function openPerms() {
    $('.frame').attr("src", "playerperms.html");
}

function openNegs() {
    $('.frame').attr("src", "playernegs.html");
}