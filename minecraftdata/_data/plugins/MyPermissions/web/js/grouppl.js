load();

function load() {
    $.ajax({
        url: '',
        type: "GETGROUPS",
        success: function(result){
            $('#groupSelect').html(result);
            getPlayers($('#groupSelect').val());
            console.log(result);
        },
        error: function(error) {
            $('.panel').html('Error');
        }
    })
}

function getPlayers(group) {
    $.ajax({
        url: group,
        type: "GETGPLAYERS",
        success: function(result){
            $('#groupPlayers').html(result);
            console.log(result);
        },
        error: function(error) {
            $('.panel').html('Error');
        }
    })
    $.ajax({
        url: group,
        type: "GETOPLAYERS",
        success: function(result){
            $('#otherPlayers').html(result);
            console.log(result);
        },
        error: function(error) {
            $('.panel').html('Error');
        }
    })
}

function addPlayer() {
    var s = $('#otherPlayers').val();
    $.ajax({
        url: $('#groupSelect').val(),
        type: "ADDPLAYER",
        data: s,
        success: function(result){
            console.log(result);
            if(result == "success") {
                $('#status').attr("style","color: #00EE00FF;");
                $('#status').val("Player added!");
            } else {
                $('#status').attr("style","color: #EE0000FF;");
                $('#status').val("Adding player failed!");
            }
        },
        error: function(error) {
            $('.panel').html('Error');
        }
    })
}

function removePlayer() {
    var s = $('#groupPlayers').val();
    $.ajax({
        url: $('#groupSelect').val(),
        type: "REMOVEPLAYER",
        data: s,
        success: function(result){
            console.log(result);
            if(result == "success") {
                $('#status').attr("style","color: #00EE00FF;");
                $('#status').val("Player removed!");
            } else {
                $('#status').attr("style","color: #EE0000FF;");
                $('#status').val("Removing player failed!");
            }
        },
        error: function(error) {
            $('.panel').html('Error');
        }
    })
}