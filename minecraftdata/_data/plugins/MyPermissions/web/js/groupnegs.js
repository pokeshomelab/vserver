load();

function load() {
    $.ajax({
        url: '',
        type: "GETGROUPS",
        success: function(result){
            $('#groupSelect').html(result);
            getGroupNegs($('#groupSelect').val());
            console.log(result);
        },
        error: function(error) {
            $('.panel').html('Error');
        }
    })
}

function getGroupNegs(group) {
    $.ajax({
        url: group,
        type: "GETGNEGS",
        success: function(result){
            $('#permsPanel').val(result);
            console.log(result);
        },
        error: function(error) {
            $('.panel').html('Error');
        }
    })
}

function sendGNegs() {
    $.ajax({
        url: $('#groupSelect').val(),
        type: "SETGNEGS",
        data: $('#permsPanel').val() + '\n',
        success: function(result){
            console.log(result);
        },
        error: function(error) {
            $('.panel').html('Error');
        }
    })
    location.reload();
}