load();

function load() {
    $.ajax({
        url: '',
        type: "GETPLAYERS",
        success: function(result){
            $('#playerSelect').html(result);
            getPlayerNegs($('#playerSelect').val());
            console.log(result);
        },
        error: function(error) {
            $('.panel').html('Error');
        }
    })
}

function getPlayerNegs(player) {
    $.ajax({
        url: player,
        type: "GETPNEGS",
        success: function(result){
            $('#permsPanel').val(result);
            console.log(result);
        },
        error: function(error) {
            $('.panel').html('Error');
        }
    })
}

function sendPNegs() {
    var s = $('#permsPanel').val() + "\n";
    $.ajax({
        url: $('#playerSelect').val(),
        type: "SETPNEGS",
        data: s,
        success: function(result){
            console.log(result);
        },
        error: function(error) {
            $('.panel').html('Error');
        }
    })
    location.reload();
}